<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

//Request::setTrustedProxies(array('127.0.0.1'));

$app->get('/', function () use ($app) {
    $sql = "SELECT  
            departments.dept_name, departments.dept_no,
            COUNT(DISTINCT dept_manager.emp_no) AS manager_count,
            COUNT(DISTINCT current_dept_emp.emp_no) AS emp_count   
              FROM departments
LEFT JOIN dept_manager ON departments.dept_no=dept_manager.dept_no
LEFT JOIN current_dept_emp ON departments.dept_no=current_dept_emp.dept_no
GROUP BY departments.dept_no
";
    $departments = $app['db']->fetchAll($sql);
    return $app['twig']->render('index.html.twig', array(
        'departments' => $departments
    ));
})
->bind('homepage')
;

$app->get('/department/{id}', function (Silex\Application $app, $id) use ($app)
{
    $sql = "
SELECT dept_manager.*, employees.* FROM dept_manager
LEFT JOIN employees ON dept_manager.emp_no=employees.emp_no
WHERE dept_no = ?
";
    $managers = $app['db']->fetchAll($sql, array($id));
    $sql = "
            SELECT current_dept_emp.*, employees.* FROM current_dept_emp 
            LEFT JOIN employees ON current_dept_emp.emp_no=employees.emp_no
            WHERE dept_no = ? 
            LIMIT 40
            ";
    $employees = $app['db']->fetchAll($sql, array($id));

    return $app['twig']->render('department.html.twig', array(
        'managers' => $managers,
        'employees' => $employees
    ));

});

$app->get('/employe/{id}', function (Silex\Application $app, $id) use ($app)
{

    $manager = $app['db']->fetchAssoc("SELECT * FROM dept_manager WHERE emp_no = ?", array($id));

    $sql = "
            SELECT current_dept_emp.*, employees.* FROM current_dept_emp 
            LEFT JOIN employees ON current_dept_emp.emp_no=employees.emp_no
            WHERE current_dept_emp.emp_no = ?
            ";
    $employe = $app['db']->fetchAssoc($sql, array($id));

    $sql = "SELECT * FROM salaries WHERE emp_no = ? ORDER BY from_date";
    $salary = $app['db']->fetchAll($sql, array($id));

    $sql = "SELECT * FROM titles WHERE emp_no = ? ORDER BY from_date";
    $positions = $app['db']->fetchAll($sql, array($id));

    $sql = "SELECT dept_emp.*,  departments.dept_name FROM dept_emp 
            LEFT JOIN departments ON dept_emp.dept_no=departments.dept_no
WHERE emp_no = ? ORDER BY from_date";
    $departments = $app['db']->fetchAll($sql, array($id));
    return $app['twig']->render('employe.html.twig', array(
        'employe' => $employe,
        'salary' => $salary,
        'positions' => $positions,
        'departments' => $departments,
        'manager' => $manager
    ));

});

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html.twig',
        'errors/'.substr($code, 0, 2).'x.html.twig',
        'errors/'.substr($code, 0, 1).'xx.html.twig',
        'errors/default.html.twig',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});
