<?php
 return array (
    'mysql_read' => array(
        'driver'    => 'pdo_mysql',
        'host'      => 'localhost',
        'dbname'    => 'employees',
        'user'      => 'root',
        'password'  => '',
        'charset'   => 'utf8',
    ),
    'mysql_write' => array(
        'driver'    => 'pdo_mysql',
        'host'      => 'localhost',
        'dbname'    => 'employees',
        'user'      => 'root',
        'password'  => '',
        'charset'   => 'utf8',
    ),
);
